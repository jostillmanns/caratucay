// Google Analytics
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-36464707-1']);
_gaq.push(['_trackPageview']);

(function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();


// Website stuff
sinnflut_2009_array = new Array("01.jpg", "02.jpg", "03.jpg", "04.jpg", "05.jpg", "06.jpg", "07.jpg", "08.jpg", "09.jpg", "10.jpg", "11.jpg", "12.jpg", "13.jpg");
sinnflut_2009_index = 0;

feldmannstiftung_array = new Array('Andi 01.JPG','ANdi 02.JPG','Andi 03.JPG','Andi + Lutz 01.JPG','Andi + Lutz 02.JPG','Joschka 01.JPG','Joschka 02.JPG','Joschka 03.JPG','Joschka 04.JPG','Joschka 05.JPG','Lutz 01.JPG','Micha 01.JPG','Micha 03.JPG','Micha 04.JPG','Micha 05.JPG','Micha 06.JPG','Micha + Joschka 01.JPG','Oggy 01.JPG','Oggy 02.JPG','Oggy 03.JPG','Oggy 04.JPG','Oggy + Lutz 01.JPG','Oggy + Micha 01.JPG');
feldmannstiftung_index = 0;

rdd_array = new Array('01.jpg', '02.jpg', '03.jpg', '04.jpg', '05.jpg', '06.jpg', '07.jpg', '08.jpg', '09.jpg', '10.jpg' );
rdd_index = 0;

sinnflut_2010_array = new Array('Alle.JPG', 'Andi 01.JPG', 'Andi 02.JPG', 'Andi 03.JPG', 'Joschka 01.JPG', 'Joschka 02.JPG', 'Lutz 01.JPG', 'Lutz 02.JPG', 'Lutz 03.JPG', 'Lutz 04.JPG', 'Micha 01.JPG', 'Micha 02.JPG', 'Micha + Joschka 01.JPG', 'Oggy 01.JPG', 'Oggy 02.JPG', 'Oggy 03.JPG', 'Oggy 04.JPG', 'Oggy + Lutz 01.JPG', 'Oggy + Lutz 02.JPG', 'Oggy + Lutz 03.JPG');
sinnflut_2010_index = 0;

promo_array = new Array('10-Lara.jpg', '127-Lara.jpg', '127-Lara_runterskaliert.jpg', '135-Lara.jpg', '136-Lara.jpg', '142-Andy.jpg', '145-Lara.jpg', '157-Lara.jpg', '163-Lara.jpg', '165-Lara.jpg', '37-Lara.jpg', '58-Lara.jpg', '83-Lara.jpg' );
promo_index = 0;

function prev(filename_array, index, foldername, html_element)
{
    if (index != 0) {
	index--;
	document.getElementById(html_element).innerHTML='<img src="gallery/' + foldername + '/' + filename_array[index] + '" />';
    }
    return index;
}

function next(filename_array, index, foldername, html_element)
{
    if (++index < filename_array.length) {
    	document.getElementById(html_element).innerHTML='<img src="gallery/' + foldername + '/' + filename_array[index] + '" />';
    }
    return index;
}

$(document).ready(function(){
    $('#top_header').load('top_header.html');
    $('#top_nav').load('top_nav.html');
    $('#footer_info').load('footer_info.html');

    $('#prev_sinnflut_2009').click(function(){
	sinnflut_2009_index = prev(sinnflut_2009_array, sinnflut_2009_index, 'sinnflut_2009', 'picture_sinnflut_2009');	
	
    });
    $('#next_sinnflut_2009').click(function(){
	sinnflut_2009_index = next(sinnflut_2009_array, sinnflut_2009_index, 'sinnflut_2009', 'picture_sinnflut_2009');
    });

    $('#prev_feldmannstiftung').click(function(){
	feldmannstiftung_index = prev(feldmannstiftung_array, feldmannstiftung_index, 'feldmannstiftung', 'picture_feldmannstiftung');	
    });
    $('#next_feldmannstiftung').click(function(){
	feldmannstiftung_index = next(feldmannstiftung_array, feldmannstiftung_index, 'feldmannstiftung', 'picture_feldmannstiftung');	
    })
    $('#prev_rdd').click(function(){
	rdd_index = prev(rdd_array, rdd_index, 'rdd', 'picture_rdd');	
    });
    $('#next_rdd').click(function(){
	rdd_index = next(rdd_array, rdd_index, 'rdd', 'picture_rdd');	
    });
    $('#prev_sinnflut_2010').click(function(){
	sinnflut_2010_index = prev(sinnflut_2010_array, sinnflut_2010_index, 'sinnflut_2010', 'picture_sinnflut_2010');		
    });
    $('#next_sinnflut_2010').click(function(){
	sinnflut_2010_index = next(sinnflut_2010_array, sinnflut_2010_index, 'sinnflut_2010', 'picture_sinnflut_2010');
    });
    $('#prev_promo').click(function(){
	promo_index = prev(promo_array, promo_index, 'promo', 'picture_promo');		
    });
    $('#next_promo').click(function(){
	promo_index = next(promo_array, promo_index, 'promo', 'picture_promo');		
    });

    // Music open in new Window
    $('#song_face').click(function(){
	window.open('http://www.youtube.com/watch?v=f_jXFfDAKWs');
    });
    $('#song_schizo').click(function(){
	window.open('http://www.youtube.com/watch?v=5qRADMUgJvo');
    });
    $('#song_part1').click(function(){
	window.open('http://www.youtube.com/watch?v=Tn1DWqojv-0');	
    });

    $('#song_voab').click(function(){
	window.open('http://www.youtube.com/watch?v=VfI0LgO3K5M');
    });
    $('#song_ghost').click(function(){
	window.open('http://www.youtube.com/watch?v=WuYfegFDLuI');
    });
    $('#song_oblivion').click(function(){
	window.open('http://www.youtube.com/watch?v=emnrJlOR1VU');	
    });
});

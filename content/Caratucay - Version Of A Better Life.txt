Version Of A Better Life
 
This is me telling you the story of life
Your goals and wishes condemned to fail
 
Your time of duty is over, you're free
But think carefully, it's just a dream
 
You think the shit at school is done
but in fact you're just another one
You think life is getting easier
Using your knowledge to get rich and successful
 
Your time of duty is over, you're free
But think carefully, it's just a dream
 
The way gets smaller every step you go
Expectations and restrictions won't allow you to grow
In your own way
As you wanted to
As you wished to
What you wanted to do
 
Your time of duty is over, you're free
But think carefully, it's just a dream
 
Enjoy your dreams and your wishes too
Let it all come over you
Working hard and hope your dream comes true
